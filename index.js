let numberOfDrum = document.querySelectorAll('.drum').length;

for (let i = 0; i < numberOfDrum; i++) {
  document.querySelectorAll('.drum')[i].addEventListener('click', function () {
    let innerButtonHTML = this.innerHTML;
    makeSound(innerButtonHTML);
    buttonAnimation(innerButtonHTML);
  });
}

document.addEventListener('keypress', diKlik);

function diKlik(event) {
  makeSound(event.key);
  buttonAnimation(event.key);
}

function makeSound(key) {
  switch (key) {
    case 'w':
      let crash = new Audio('sounds/crash.mp3');
      crash.play();

      break;

    case 'a':
      let bass = new Audio('sounds/kick-bass.mp3');
      bass.play();

      break;

    case 's':
      let snare = new Audio('sounds/snare.mp3');
      snare.play();

      break;

    case 'd':
      let tom1 = new Audio('sounds/tom-1.mp3');
      tom1.play();

      break;

    case 'j':
      let tom2 = new Audio('sounds/tom-2.mp3');
      tom2.play();

      break;

    case 'k':
      let tom3 = new Audio('sounds/tom-3.mp3');
      tom3.play();

      break;

    case 'l':
      let tom4 = new Audio('sounds/tom-4.mp3');
      tom4.play();

      break;

    default:
      console.log(innerButtonHTML);
  }
}

function buttonAnimation(currentKey) {
  let buttonActive = document.querySelector('.' + currentKey);
  buttonActive.classList.add('pressed');
  setTimeout(function () {
    buttonActive.classList.remove('pressed');
  }, 100);
}
